;;=============================================================================
;;
;; Emacs Configuration
;;
;; Themes Settings
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; Syntax Highlighting
;;-----------------------------------------------------------------------------
;; Enable font lock
(global-font-lock-mode 1)

;; Maximum colors
(setq font-lock-maximum-decoration t)

;; Highlight parentheses
(setq show-paren-delay 0
  show-paren-style 'parenthesis)
(show-paren-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Show trailing whitespace
(setq-default show-trailing-whitespace t)
(add-hook 'comint-mode-hook
          (lambda () (setq show-trailing-whitespace nil)))
(add-hook 'term-mode-hook
          (lambda () (setq show-trailing-whitespace nil)))


;;-----------------------------------------------------------------------------
;; visual-fill-column
;;-----------------------------------------------------------------------------
(use-package visual-fill-column
  :disabled
  :init
  (global-visual-fill-column-mode))

;;-----------------------------------------------------------------------------
;; Color Theme
;;-----------------------------------------------------------------------------
(use-package color-theme-sanityinc-tomorrow
  :disabled
  :ensure
  :config
  (load-theme 'sanityinc-tomorrow-night t))

(use-package base16-theme
  :ensure
  :config
  (load-theme 'base16-monokai t))

;;-----------------------------------------------------------------------------
;; Smart Mode Line
;;-----------------------------------------------------------------------------
;; https://github.com/Bruce-Connor/smart-mode-line
(use-package smart-mode-line
  :ensure
  :config
  ;; Select theme: dark, light, respectful
  (setq sml/theme 'respectful)
  (setq sml/no-confirm-load-theme t)

  ;; Enable smart-mode-line
  (sml/setup)

  ;; Theme
  (sml/apply-theme 'respectful)

  ;; Minor modes to hide
  (add-to-list 'sml/hidden-modes " yas")
  (add-to-list 'sml/hidden-modes " Undo-Tree")
  (add-to-list 'sml/hidden-modes " vl")
  (add-to-list 'sml/hidden-modes " Wrap")
  (add-to-list 'sml/hidden-modes " SP")
  (add-to-list 'sml/hidden-modes " AC")
  (add-to-list 'sml/hidden-modes " Fill")
  (add-to-list 'sml/hidden-modes " pair")
  (add-to-list 'sml/hidden-modes " company")
  (add-to-list 'sml/hidden-modes " Ind"))

;;-----------------------------------------------------------------------------
;; Fringe Bitmaps
;;-----------------------------------------------------------------------------
(define-fringe-bitmap 'right-curly-arrow
  [#b00000000
   #b00000000
   #b00000000
   #b00000000
   #b01110000
   #b00010000
   #b00010000
   #b00000000])

(define-fringe-bitmap 'left-curly-arrow
  [#b00000000
   #b00001000
   #b00001000
   #b00001110
   #b00000000
   #b00000000
   #b00000000
   #b00000000])

;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-themes)
