;;=============================================================================
;;
;; Emacs Configuration
;;
;; Packages
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; ELPA
;;-----------------------------------------------------------------------------
(require 'package)

(add-to-list
 'package-archives
 ;; '("gnu" . "http://elpa.gnu.org/packages/")
 ;; '("marmalade" . "http://marmalade-repo.org/packages/")
 ;; '("melpa-stable" . "http://melpa-stable.milkbox.net/packages/")
 '("melpa" . "http://melpa.org/packages/")
 t)

(package-initialize)

;;-----------------------------------------------------------------------------
;; List of Packages
;;-----------------------------------------------------------------------------

(defvar conf-packages-base '(
  use-package
))


;;-----------------------------------------------------------------------------
;; Install Packages
;;-----------------------------------------------------------------------------

(defun conf-install (packages)
  (mapc (lambda (package)
          (when (not (package-installed-p package))
            (package-install package)))
        packages))

(defun conf-packages-bootstrap ()
  (if (not (require 'use-package nil t))
      ;; Then: Bootstrap?
      (when (y-or-n-p (format "Base packages missing. Bootstrap them?"))
        (progn
          (package-refresh-contents)
          (conf-install conf-packages-base)
          (require 'use-package)))
    ;; Else: do nothing
    (progn)))

;; Bootstrap if needed
(conf-packages-bootstrap)

;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-packages)
