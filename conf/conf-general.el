;;=============================================================================
;;
;; Emacs Configuration
;;
;; General Settings
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; Interface Settings
;;-----------------------------------------------------------------------------
;; disable start-up screen
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)

;; disable scrollbar
(if (fboundp 'scroll-bar-mode)
    (scroll-bar-mode -1))

;; disable toolbar
(if (fboundp 'tool-bar-mode)
    (tool-bar-mode -1))

;; disable menubar
(if (fboundp 'menu-bar-mode)
    (menu-bar-mode -1))

;; tooltip
(if (fboundp 'tooltip-mode)
    (tooltip-mode -1))
;;(setq tooltip-use-echo-area t)

;; disable system bell
(setq ring-bell-function 'ignore)

;; transparency
;(set-frame-parameter (selected-frame) 'alpha 85)

;; frame title
;; (setq frame-title-format "emacs")
(setq frame-title-format
      '((:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))


;; initial message
(setq initial-scratch-message nil)

;; scroll wheel
(setq scroll-step 1)
(setq scroll-conservatively 10000)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)

;; Disable open link on click
(setq mouse-1-click-follows-link nil)

;;-----------------------------------------------------------------------------
;; General Settings
;;-----------------------------------------------------------------------------
;; enable disabled commands
(setq disabled-command-function nil)

;; ask before closing emacs
(setq confirm-kill-emacs 'yes-or-no-p)

;; y-or-n questions
(defalias 'yes-or-no-p 'y-or-n-p)

;; transient mark
(setq transient-mark-mode t)

;; line and column positions
(setq line-number-mode t)
(setq column-number-mode t)

;; fill column width
(set-default 'fill-column 80)

;; balancing window heights
(setq even-window-heights nil)

;; save clipboard strings into kill ring before replacing them
(setq save-interprogram-paste-before-kill t)

;; add final newline character according to mode
(setq mode-require-final-newline t)

;;-----------------------------------------------------------------------------
;; Spacing
;;-----------------------------------------------------------------------------
;; no tabs
(setq-default indent-tabs-mode nil)

;; default indentation at 2 spaces
(setq standard-indent 2)

;; word wrapping
(setq-default word-wrap t)
(global-visual-line-mode -1)
(setq truncate-partial-width-windows nil)

;;-----------------------------------------------------------------------------
;; UTF-8
;;-----------------------------------------------------------------------------
(prefer-coding-system 'utf-8)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

;;-----------------------------------------------------------------------------
;; Backups and Auto-saves
;;-----------------------------------------------------------------------------
;; (defvar backup-directory "~/.emacs.d/backup/")
;; (setq backup-by-copying t)
;; (setq backup-directory-alist
;;       `((".*" . ,backup-directory)))
;; (setq auto-save-file-name-transforms
      ;; `((".*" ,backup-directory t)))

(setq backup-inhibited t)
(setq auto-save-default nil)
(setq create-lockfiles nil)

;;-----------------------------------------------------------------------------
;; Abbrevs
;;-----------------------------------------------------------------------------
(setq abbrev-file-name "~/.emacs.d/abbrev_defs")

;;-----------------------------------------------------------------------------
;; Visit file in read-only by default
;;-----------------------------------------------------------------------------
;; (add-hook 'find-file-hook
;;   '(lambda ()
;;      (when (and (buffer-file-name)
;;         (file-exists-p (buffer-file-name))
;;         (file-writable-p (buffer-file-name)))
;;        (message "Toggle to read-only for existing file")
;;        (toggle-read-only 1))))


;;-----------------------------------------------------------------------------
;; Environment Variables
;;-----------------------------------------------------------------------------
;; (use-package exec-path-from-shell
;;   :ensure
;;   :demand
;;   :config
;;   (dolist (var '("SSH_AUTH_SOCK" "SSH_AGENT_PID" "GPG_AGENT_INFO"
;;                  "LANG"
;;                  "NAME" "EMAIL"
;;                  "GIT_AUTHOR_NAME" "GIT_AUTHOR_EMAIL"
;;                  "GIT_COMMITTER_NAME" "GIT_COMMITTER_EMAIL"))
;;     (add-to-list 'exec-path-from-shell-variables var)
;;     (exec-path-from-shell-initialize)))


;;-----------------------------------------------------------------------------
;; epa-file
;;-----------------------------------------------------------------------------
(use-package epa-file
  :config
  (setq epa-file-select-keys nil)
  (setq epa-file-encrypt-to t)
  (setq epa-file-cache-passphrase-for-symmetric-encryption t)
  (setenv "GPG_AGENT_INFO" nil))


;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-general)
