;;=============================================================================
;;
;; Emacs Configuration
;;
;; Minor Modes
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; completion
;;-----------------------------------------------------------------------------
(use-package vertico
  :ensure
  :init
  (vertico-mode))

(use-package orderless
  :ensure
  :init
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))

(use-package marginalia
  :ensure
  :init
  (marginalia-mode))

(use-package embark
  :ensure

  :bind
  (("C-." . embark-act)
   ("C-;" . embark-dwim))

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  :config
  (setq embark-prompter 'embark-keymap-prompter)
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package consult
  :ensure
  :init
  (defun conf-consult-ripgrep-symbol-at-point ()
    (interactive)
    (let ((pat (thing-at-point 'symbol))
          (dir (ignore-errors (vc-root-dir))))
      (consult-ripgrep dir pat)))

  (defun conf-consult-line-symbol-at-point ()
    (interactive)
    (consult-line (thing-at-point 'symbol)))

  :bind
  ("C-c h" . consult-history)
  ("C-c m" . consult-mode-command)
  ("C-c b" . consult-bookmark)
  ("C-c k" . consult-kmacro)
  ("C-x b" . consult-buffer)
  ("C-x 4 b" . consult-buffer-other-window)
  ("C-x 5 b" . consult-buffer-other-frame)
  ("M-#" . consult-register-load)
  ("M-'" . consult-register-store)
  ("C-M-#" . consult-register)
  ("M-y" . consult-yank-pop)
  ("M-s M-f" . consult-find)
  ("M-s M-s" . conf-consult-line-symbol-at-point)
  ("M-s M-r" . conf-consult-ripgrep-symbol-at-point)

  :config
  (setq consult-project-root-function #'vc-root-dir)
  (setq consult-ripgrep-command
        "rg --null --smart-case --line-buffered --color=ansi --max-columns=1000 --no-heading --line-number . -e ARG OPTS"))


;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure
  :after (embark consult)
  :demand
  ;; if you want to have consult previews as you move around an
  ;; auto-updating embark collect buffer
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;;-----------------------------------------------------------------------------
;; eglot
;;-----------------------------------------------------------------------------
(use-package eglot
  :ensure
  :bind (:map eglot-mode-map
	      ("C-c l a" . eglot-code-actions)
              ("C-c l d" . xref-find-definitions)
              ("C-c l h" . eldoc)
              ("C-c l f" . xref-find-references)
	      ("C-c l r" . eglot-rename)))

;;-----------------------------------------------------------------------------
;; active-region
;;-----------------------------------------------------------------------------
(use-package active-region
  :demand
  :init
  (defun active-region-on ()
    (active-region-mode 1))

  (defun active-region-off ()
    (active-region-mode -1))

  (add-hook 'activate-mark-hook 'active-region-on)
  (add-hook 'deactivate-mark-hook 'active-region-off))

;;-----------------------------------------------------------------------------
;; selected
;;-----------------------------------------------------------------------------
(use-package selected
  :disabled
  :ensure nil
  :demand
  :commands selected-minor-mode
  :init
  (selected-global-mode 1)
  (setq selected-org-mode-map (make-sparse-keymap))

  ;; http://stackoverflow.com/a/32002122
  (defun conf-isearch-with-region ()
    "Use region as the isearch text."
    (when mark-active
      (let ((region (funcall region-extract-function nil)))
        (deactivate-mark)
        (isearch-push-state)
        (isearch-yank-string region))))

  (add-hook 'isearch-mode-hook #'conf-isearch-with-region)

  :bind (:map selected-keymap
              ("q" . selected-off)
              ("u" . upcase-region)
              ("d" . downcase-region)
              ("w" . count-words-region)
              ("m" . apply-macro-to-region-lines)
              :map selected-org-mode-map
              ("t" . org-table-convert-region)))

;;-----------------------------------------------------------------------------
;; uniquify
;;-----------------------------------------------------------------------------
(use-package uniquify
  :init
  (setq uniquify-buffer-name-style 'post-forward)
  (setq uniquify-separator ":"))

;;-----------------------------------------------------------------------------
;; winner
;;-----------------------------------------------------------------------------
(winner-mode 1)

;;-----------------------------------------------------------------------------
;; windmove
;;-----------------------------------------------------------------------------
(windmove-default-keybindings)

;;-----------------------------------------------------------------------------
;; Flycheck
;;-----------------------------------------------------------------------------
(use-package flycheck
  :ensure
  :init
  (add-hook 'after-init-hook #'global-flycheck-mode))

;;-----------------------------------------------------------------------------
;; Undo Tree
;;-----------------------------------------------------------------------------
(use-package undo-tree
  :ensure
  :config
  (setq undo-tree-auto-save-history nil)
  :init
  (global-undo-tree-mode))

;;-----------------------------------------------------------------------------
;; popwin
;;-----------------------------------------------------------------------------
;; https://github.com/m2ym/popwin-el
(use-package popwin
  :disabled t
  :ensure
  :config
  (setq display-buffer-function 'popwin:display-buffer)

  (setq popwin:popup-window-height 10)

  ;; Capture Helm Windows
  (push `("^\*helm[ -].+\*$" :regexp t :height ,popwin:popup-window-height)
        popwin:special-display-config))

;;-----------------------------------------------------------------------------
;; electric-indent-mode
;;-----------------------------------------------------------------------------
(electric-indent-mode -1)

;;-----------------------------------------------------------------------------
;; electric-pair-mode
;;-----------------------------------------------------------------------------
(electric-pair-mode t)

;;-----------------------------------------------------------------------------
;; yasnippet
;;-----------------------------------------------------------------------------
(use-package yasnippet
  :ensure
  :config
  (yas-global-mode 1)

  ;; Custom Snippets
  ;; (setq yas-snippet-dirs "~/.emacs.d/snippets")

  (define-key yas-minor-mode-map [(tab)] nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  (define-key yas-minor-mode-map (kbd "<C-tab>") 'yas-expand)

  ;; Don't enable yasnippet in term-mode
  (add-hook 'term-mode-hook (lambda ()
                              (setq yas-dont-activate t))))

;;-----------------------------------------------------------------------------
;; Rainbow Delimiters
;;-----------------------------------------------------------------------------
;; https://github.com/jlr/rainbow-delimiters
(use-package rainbow-delimiters
  :ensure
  :init
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

;;-----------------------------------------------------------------------------
;; VC Mode
;;-----------------------------------------------------------------------------
;; Disable
;; (setq vc-handled-backends nil)

;;-----------------------------------------------------------------------------
;; vlf
;;-----------------------------------------------------------------------------
;; https://github.com/m00natic/vlfi
(use-package vlf
  :ensure)

;;-----------------------------------------------------------------------------
;; whole-line-or-region
;;-----------------------------------------------------------------------------
(use-package whole-line-or-region
  :ensure)

;;-----------------------------------------------------------------------------
;; Unfill
;;-----------------------------------------------------------------------------
;; https://github.com/purcell/unfill
(use-package unfill
  :ensure
  :bind
  ("M-Q" . unfill-region))

;;-----------------------------------------------------------------------------
;; Expand Region
;;-----------------------------------------------------------------------------
;; https://github.com/magnars/expand-region.el
(use-package expand-region
  :ensure
  :bind
  ("C-=" . er/expand-region))

;;-----------------------------------------------------------------------------
;; fold-this
;;-----------------------------------------------------------------------------
;; https://github.com/magnars/fold-this.el
(use-package fold-this
  :ensure
  :bind
  (("C-, C-d" . fold-this)
   ("C-, C-S-d" . fold-this-unfold-all)))

;;-----------------------------------------------------------------------------
;; Delete Selection Mode
;;-----------------------------------------------------------------------------
(pending-delete-mode t)

;;-----------------------------------------------------------------------------
;; Company Mode
;;-----------------------------------------------------------------------------
(use-package company
  :ensure
  :custom
  (company-idle-delay nil)
  (company-tooltip-idle-delay nil)
  (company-begin-commands nil)
  (company-dabbrev-downcase nil)
  :init
  (global-company-mode)
  :bind
  ("M-/" . company-complete)
  (:map company-active-map
	("C-n" . company-select-next)
	("C-p" . company-select-previous)
	("M-<" . company-select-first)
	("M->" . company-select-last)))

;;-----------------------------------------------------------------------------
;; git-gutter
;;-----------------------------------------------------------------------------
(use-package git-gutter
  :ensure
  :config
  (global-git-gutter-mode 1))

;;-----------------------------------------------------------------------------
;; git-auto-commit-mode
;;-----------------------------------------------------------------------------
(use-package git-auto-commit-mode
  :ensure)

;;-----------------------------------------------------------------------------
;; move-text
;;-----------------------------------------------------------------------------
(use-package move-text
  :ensure
  :config
  (move-text-default-bindings))

;;-----------------------------------------------------------------------------
;; multiple-cursors
;;-----------------------------------------------------------------------------
;; https://github.com/magnars/multiple-cursors.el
(use-package multiple-cursors
  :ensure
  :init
  (define-key active-region-mode-map (kbd "<C-return>") 'mc/edit-lines)
  ;; (define-key selected-keymap (kbd "<C-return>") 'mc/edit-lines)
  :bind
  (("C-, C-o" . mc/mark-all-dwim)))

;;-----------------------------------------------------------------------------
;; wgrep
;;-----------------------------------------------------------------------------
(use-package wgrep
  :ensure
  :config
  (setq wgrep-auto-save-buffer t))

;;-----------------------------------------------------------------------------
;; buffer-move
;;-----------------------------------------------------------------------------
(use-package buffer-move
  :ensure
  :bind
  (("C-, <up>" . buf-move-up)
   ("C-, <down>" . buf-move-down)
   ("C-, <left>" . buf-move-left)
   ("C-, <right>" . buf-move-right)))

;;-----------------------------------------------------------------------------
;; beginend
;;-----------------------------------------------------------------------------
(use-package beginend
  :disabled t
  :ensure
  :config
  (beginend-global-mode))

;;-----------------------------------------------------------------------------
;; smart-comment
;;-----------------------------------------------------------------------------
(use-package smart-comment
  :ensure
  :bind
  (("M-;" . smart-comment)))

;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-minor)
