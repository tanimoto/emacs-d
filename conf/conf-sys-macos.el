;;=============================================================================
;;
;; Emacs Configuration
;;
;; Mac OS Settings
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; Default Settings
;;-----------------------------------------------------------------------------

;; Use command key as meta
(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'meta)

;; Position and Size
(add-to-list 'default-frame-alist '(left . 0))
(add-to-list 'default-frame-alist '(top . 0))
(add-to-list 'default-frame-alist '(height . 32))
(add-to-list 'default-frame-alist '(width . 80))

;; Transparency
;; (set-frame-parameter (selected-frame) 'alpha '(90 90))
;; (add-to-list 'default-frame-alist '(alpha 90 90))

;; Set default font
(custom-set-faces '(default ((t
  (:inherit nil
   :stipple nil
   ;; :background "#1d1f21"
   ;; :foreground "#c5c8c6"
   :inverse-video nil
   :box nil
   :strike-through nil
   :overline nil
   :underline nil
   :slant normal
   :weight normal
   :height 170
   :width normal
   :foundry "apple"
   :family "Monaco")))))


;; Shell
;; (setq explicit-shell-file-name "/bin/bash")


;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-sys-macos)
