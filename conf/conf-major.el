;;=============================================================================
;;
;; Emacs Configuration
;;
;; Major Modes
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; project
;;-----------------------------------------------------------------------------
(use-package project
  :ensure)

;;-----------------------------------------------------------------------------
;; Tramp Mode
;;-----------------------------------------------------------------------------
;; Enable remote sudo
;; /sudo:root@host:file
(use-package tramp
  :ensure
  :config
  (setq tramp-default-method "ssh")
  ;; Ensure PATH is preserved
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  (set-default 'tramp-default-proxies-alist
               (quote ((".*" "\\`root\\'" "/ssh:%h:")))))


;;-----------------------------------------------------------------------------
;; dired
;;-----------------------------------------------------------------------------
(use-package dired
  :config
  (define-key dired-mode-map (kbd "C-o") nil)
  (add-hook 'dired-mode-hook (lambda () (dired-hide-details-mode 1))))

;;-----------------------------------------------------------------------------
;; ediff
;;-----------------------------------------------------------------------------
(use-package ediff
  :init
  ;; use same window
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)

  ;; split horizontally
  (setq ediff-split-window-function 'split-window-horizontally)

  ;; Only highlight the current diff
  ;; (setq-default ediff-highlight-all-diffs nil)

  ;; https://emacs.stackexchange.com/questions/7482/restoring-windows-and-layout-after-an-ediff-session
  (defvar ediff-last-windows nil
    "Last ediff window configuration.")

  (defun ediff-restore-windows ()
    "Restore window configuration to `ediff-last-windows'."
    (set-window-configuration ediff-last-windows)
    (remove-hook 'ediff-after-quit-hook-internal
                 'ediff-restore-windows))

  (defadvice ediff-buffers (around ediff-restore-windows activate)
    (setq ediff-last-windows (current-window-configuration))
    (add-hook 'ediff-after-quit-hook-internal 'ediff-restore-windows)
    ad-do-it))


;;-----------------------------------------------------------------------------
;; Org Mode
;;-----------------------------------------------------------------------------
;; (use-package org-install
;;   :config
;;   (add-hook 'org-mode-hook
;;             (lambda () (define-key org-mode-map (kbd "C-,") nil)))

;;   ;; Handle conflict with Windmove
;;   (add-hook 'org-shiftup-final-hook 'windmove-up)
;;   (add-hook 'org-shiftleft-final-hook 'windmove-left)
;;   (add-hook 'org-shiftdown-final-hook 'windmove-down)
;;   (add-hook 'org-shiftright-final-hook 'windmove-right)

;;   ;; Enable speed keys
;;   (setq org-use-speed-commands t)

;;   ;; Indent levels
;;   (setq org-startup-indented nil)

;;   ;; Todo dependencies
;;   (setq org-enforce-todo-dependencies t)

;;   ;; Todo Keywords
;;   (setq org-todo-keywords
;;         ;; '((sequence "TODO(t)" "WAIT(w!)" "|" "DONE(d!)" "CANCELED(c!)")))
;;         '((sequence "TODO(t)" "WAIT(w)" "|" "DONE(d)" "CANCELED(c)")))

;;   ;; Track changes in Todo
;;   (setq org-log-done 'time)

;;   ;; Notes File
;;   (setq org-default-notes-file
;;         (expand-file-name "~/memo/memo.org")))

;;-----------------------------------------------------------------------------
;; Magit
;;-----------------------------------------------------------------------------
(use-package magit
  :ensure
  :config
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (setq git-commit-summary-max-length 72)
  ;; (magit-wip-after-save-mode t)
  ;; (magit-wip-after-apply-mode t)
  :bind
  (("C-, C-v" . magit-status)))

;;-----------------------------------------------------------------------------
;; git-link
;;-----------------------------------------------------------------------------
(use-package git-link
  :ensure)

;;-----------------------------------------------------------------------------
;; git-timemachine
;;-----------------------------------------------------------------------------
(use-package git-timemachine
  :ensure)

;;-----------------------------------------------------------------------------
;; with-editor
;;-----------------------------------------------------------------------------
(use-package with-editor
  :disabled
  :init
  (add-hook 'shell-mode-hook  'with-editor-export-editor)
  (add-hook 'term-mode-hook   'with-editor-export-editor)
  (add-hook 'eshell-mode-hook 'with-editor-export-editor))

;;-----------------------------------------------------------------------------
;; sudo-edit
;;-----------------------------------------------------------------------------
(use-package sudo-edit
  :ensure)


;;-----------------------------------------------------------------------------
;; Haskell Mode
;;-----------------------------------------------------------------------------
(use-package haskell-mode
  :ensure)

;;-----------------------------------------------------------------------------
;; text-mode
;;-----------------------------------------------------------------------------
(add-hook 'text-mode-hook 'turn-on-visual-line-mode)

;;-----------------------------------------------------------------------------
;; Markdown Mode
;;-----------------------------------------------------------------------------
(use-package markdown-mode
  :ensure
  :init
  (add-to-list 'auto-mode-alist '("\\.markdown$" . markdown-mode)))


;;-----------------------------------------------------------------------------
;; adoc-mode
;;-----------------------------------------------------------------------------
;; https://github.com/sensorflo/adoc-mode
(use-package adoc-mode
  :ensure
  :init
  (add-to-list 'auto-mode-alist '("\\.asciidoc$" . adoc-mode))
  (add-hook 'adoc-mode-hook (lambda() (buffer-face-mode t))))


;;-----------------------------------------------------------------------------
;; json-mode
;;-----------------------------------------------------------------------------
(use-package json-mode
  :ensure)


;;-----------------------------------------------------------------------------
;; yaml-mode
;;-----------------------------------------------------------------------------
(use-package yaml-mode
  :ensure)


;;-----------------------------------------------------------------------------
;; go-mode
;;-----------------------------------------------------------------------------
(use-package go-mode
  :ensure
  :hook
  (go-mode . eglot-ensure)
  :init
  (setq gofmt-command "goimports")
  (add-hook 'go-mode-hook
            (lambda ()
              (add-hook 'before-save-hook 'gofmt-before-save))))


;;-----------------------------------------------------------------------------
;; rust-mode
;;-----------------------------------------------------------------------------
(defun rustic-mode-auto-save-hook ()
  "Enable auto-saving in rustic-mode buffers."
  (when buffer-file-name
    (setq-local compilation-ask-about-save nil)))

(use-package rustic
  :ensure
  :hook
  (rustic-mode . eglot-ensure)
  (rustic-mode . rustic-mode-auto-save-hook)
  :config
  (setq rustic-lsp-client 'eglot))

(use-package cargo
  :ensure
  :hook
  (rust-mode . cargo-minor-mode))

;;-----------------------------------------------------------------------------
;; python-mode
;;-----------------------------------------------------------------------------
(use-package python
  :ensure
  :hook
  (python-mode . eglot-ensure))

;;-----------------------------------------------------------------------------
;; toml-mode
;;-----------------------------------------------------------------------------
(use-package toml-mode
  :ensure)

;;-----------------------------------------------------------------------------
;; persistent-scratch
;;-----------------------------------------------------------------------------
(use-package persistent-scratch
  :ensure
  :init
  (persistent-scratch-setup-default))

;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-major)
