;;=============================================================================
;;
;; Emacs Configuration
;;
;; Functions
;;
;;=============================================================================

;;-----------------------------------------------------------------------------
;; required
;;-----------------------------------------------------------------------------
;; Require a library if it exists and execute body
;; (defmacro required (library &rest body)
;;   `(when (require ,library nil t)
;;      (progn ,@body)))

;;-----------------------------------------------------------------------------
;; Copy line if no selection
;;-----------------------------------------------------------------------------
(defadvice kill-ring-save (before slick-copy activate compile)
  "When called interactively with no active region, copy a single line instead."
  (interactive
   (if mark-active (list (region-beginning) (region-end))
     (message "Copied line")
     (list (line-beginning-position)
           (line-beginning-position 2)))))

;;-----------------------------------------------------------------------------
;; Kill line if no selection
;;-----------------------------------------------------------------------------
(defadvice kill-region (before slick-cut activate compile)
  "When called interactively with no active region, kill a single line instead."
  (interactive
    (if mark-active (list (region-beginning) (region-end))
      (list (line-beginning-position)
        (line-beginning-position 2)))))

;;-----------------------------------------------------------------------------
;; Occur within isearch
;;-----------------------------------------------------------------------------
;; Alex Schroeder [http://www.emacswiki.org/cgi-bin/wiki/OccurBuffer]
(defun isearch-occur ()
  "*Invoke `occur' from within isearch."
  (interactive)
  (let ((case-fold-search isearch-case-fold-search))
    (occur (if isearch-regexp isearch-string (regexp-quote isearch-string)))))

;;-----------------------------------------------------------------------------
;; isearch-yank-selection
;;-----------------------------------------------------------------------------
;; https://github.com/magnars/expand-region.el/issues/17
(defun isearch-yank-selection ()
  "Put selection from buffer into search string."
  (interactive)
  (when (region-active-p)
    (deactivate-mark))
  (isearch-yank-internal (lambda () (mark))))

;;-----------------------------------------------------------------------------
;; Prelude functions
;;-----------------------------------------------------------------------------
;; https://github.com/bbatsov/prelude
(defun conf-sudo-edit (&optional arg)
  "Edit currently visited file as root.
With a prefix ARG prompt for a file to visit.
Will also prompt for a file to visit if current
buffer is not visiting a file."
  (interactive "P")
  (if (or arg (not buffer-file-name))
      (find-file (concat "/sudo:root@localhost:"
                         (ido-read-file-name "Find file(as root): ")))
    (find-alternate-file (concat "/sudo:root@localhost:" buffer-file-name))))

(defun conf-top-join-line ()
  "Join the current line with the line beneath it."
  (interactive)
  (delete-indentation 1))

(defun conf-copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

;;-----------------------------------------------------------------------------
;; Close parentheses
;;-----------------------------------------------------------------------------
;; http://www.emacswiki.org/emacs/UniversialCloseParen
;; M-x close-open-paren
;; Repeat with C-x z
(defconst all-paren-syntax-table
  (let ((table (make-syntax-table)))
    (modify-syntax-entry ?{ "(}" table)
    (modify-syntax-entry ?} "){" table)
    (modify-syntax-entry ?\( "()" table)
    (modify-syntax-entry ?\) ")(" table)
    (modify-syntax-entry ?\[ "(]" table)
    (modify-syntax-entry ?\] ")[" table)
    table)
  "A syntax table giving all parenthesis parenthesis syntax.")

(defun close-open-paren ()
  (interactive)
  (with-syntax-table all-paren-syntax-table
    (insert (save-excursion (up-list -1) (matching-paren (char-after))))))

;;-----------------------------------------------------------------------------
;; find-tag from parent
;;-----------------------------------------------------------------------------
;; https://www.reddit.com/r/emacs/comments/2nn4oo/tips_on_using_ctags_with_emacs/
(defadvice find-tag (before find-tags-table () activate)
  "find-tag (M-.) will load ./TAGS by default, the first time you use
it.  This will look in parent dirs up to root for it as well."
  (or (get-buffer "TAGS")
      (let ((tagfile (concat (locate-dominating-file (buffer-file-name) "TAGS") "TAGS")))
        (if tagfile
            (visit-tags-table tagfile)
          (error "Can't find TAGS looking upwards from %s" default-directory)))))

;;-----------------------------------------------------------------------------
;; Unwrap lines
;;-----------------------------------------------------------------------------
;; (defun unwrap-line ()
;;   "Remove all newlines until we get to two consecutive ones.
;;     Or until we reach the end of the buffer.
;;     Great for unwrapping quotes before sending them on IRC."
;;   (interactive)
;;   (let ((start (point))
;;         (end (copy-marker (or (search-forward "\n\n" nil t)
;;                               (point-max))))
;;         (fill-column (point-max)))
;;     (fill-region start end)
;;     (goto-char end)
;;     (newline)
;;     (goto-char start)))
;; (global-set-key (kbd "M-Q") 'unwrap-line)

;;-----------------------------------------------------------------------------
;; server-done for clients
;;-----------------------------------------------------------------------------
;; If buffer has client, server-done first
;; (add-hook 'server-switch-hook
;;   (lambda ()
;;     (local-set-key
;;      (kbd "C-x k")
;;      '(lambda ()
;;         (interactive)
;;         (if server-buffer-clients
;;             (server-edit)
;;           (kill-buffer))))))

;;-----------------------------------------------------------------------------
;; re-builder query replace
;;-----------------------------------------------------------------------------
;; http://www.emacswiki.org/emacs/ReBuilder
(defun reb-query-replace (to-string)
  "Replace current RE from point with `query-replace-regexp'."
  (interactive
   (progn (barf-if-buffer-read-only)
          (list (query-replace-read-to (reb-target-binding reb-regexp)
                                       "Query replace"  t))))
  (with-current-buffer reb-target-buffer
    (query-replace-regexp (reb-target-binding reb-regexp) to-string)))


;;-----------------------------------------------------------------------------
;; Change frame font height
;;-----------------------------------------------------------------------------
;; http://stackoverflow.com/questions/294664/how-to-set-the-font-size-in-emacs
(defun conf-increase-frame-font-height ()
  "Increase font height for the current frame."
  (interactive)
  (let ((old-face-attribute (face-attribute 'default :height)))
    (set-face-attribute 'default nil :height (+ old-face-attribute 10))))

(defun conf-decrease-frame-font-height ()
  "Decrease font height for the current frame."
  (interactive)
  (let ((old-face-attribute (face-attribute 'default :height)))
    (set-face-attribute 'default nil :height (- old-face-attribute 10))))

(defun conf-set-frame-font-height (height)
  "Set frame font height to HEIGHT."
  (interactive
   (list
    (let ((old-height (face-attribute 'default :height)))
     (read-number "Font height: " old-height))))
  (set-face-attribute 'default nil :height height))

;;-----------------------------------------------------------------------------
;; URL Encoding
;;-----------------------------------------------------------------------------
;; http://stackoverflow.com/questions/611831/how-to-url-decode-a-string-in-emacs-lisp
(defun conf-map-region (f start end)
  "Map a function F over the region between START and END in current buffer."
  (save-excursion
    (let ((text (delete-and-extract-region start end)))
      (insert (funcall f text)))))

(defun conf-url-encode-region (start end)
  "URL-encode the region between START and END in current buffer."
  (interactive "r")
  (conf-map-region #'url-hexify-string start end))

(defun conf-url-decode-region (start end)
  "URL-decode the region between START and END in current buffer."
  (interactive "r")
  (conf-map-region #'url-unhex-string start end))


;;-----------------------------------------------------------------------------
;; Reformat buffer using external program
;;-----------------------------------------------------------------------------
(use-package reform
  :demand)


;;-----------------------------------------------------------------------------
;; Provide
;;-----------------------------------------------------------------------------
(provide 'conf-functions)
;;; conf-functions.el ends here
