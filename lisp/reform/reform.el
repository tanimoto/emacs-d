;;; reform.el --- reformat buffer using an external command    -*- lexical-binding: t; -*-

;;; Commentary:
;;
;; Call `(reform-current-buffer BIN ARGS)' to format the current buffer using an
;; external program BIN with optional arguments ARGS.
;;
;; It is convenient to bind it to a key, such as:
;;
;;    (define-key json-mode-map (kbd "C-c C-f")
;;        (defun reform-json-buffer ()
;;          (interactive)
;;          (reform-current-buffer "jq" ".")))
;;
;; Alternatively, run reform before saving buffers:
;;
;;    (add-hook 'json-mode-hook
;;       (reform-on-save "jq" "."))
;;
;; Errors and warnings will be visible in the `*reform*' buffer.

;;; Code:

(defgroup reform nil
  "Reformat buffers using an external command."
  :group 'convenience
  :prefix "reform-")

(defcustom reform-popup-errors nil
  "Display error buffer when reform fails."
  :type 'boolean)

(defun reform-buffer (buf bin &rest args)
  "Reformat a BUF by calling an external process BIN with optional arguments ARGS."
  (unless (executable-find bin)
    (error "Could not locate executable \"%s\"" bin))

  (with-current-buffer (get-buffer-create "*reform*")
    (erase-buffer)
    (insert-buffer-substring buf)
    (let ((result (apply 'call-process-region
                         (append `(,(point-min) ,(point-max) ,bin t t nil) args))))
      (if (zerop result)
          (progn (copy-to-buffer buf (point-min) (point-max))
                 (kill-buffer))
        (when reform-popup-errors
          (display-buffer (current-buffer)))
        (error "Reform failed, see *reform* buffer for details")))))

;;;###autoload
(defun reform-current-buffer (bin &rest args)
  "Reformat the current buffer with program BIN and optional arguments ARGS.

For example:

    (define-key json-mode-map (kbd \"C-c C-f\")
        (defun reform-json-buffer ()
          (interactive)
          (reform-current-buffer \"jq\" \".\")))"
  (let ((cur-point (point))
        (cur-win-start (window-start)))
    (apply 'reform-buffer
           (append `(,(current-buffer) ,bin) args))
    (goto-char cur-point)
    (set-window-start (selected-window) cur-win-start))
  (message "Reformatted current buffer."))

;;;###autoload
(defun reform-on-save (bin &rest args)
  "Generate a function to reformat on save with program BIN and arguments ARGS.

For example:

    (add-hook 'json-mode-hook
          (reform-on-save \"jq\" \".\"))"
  (defun reform-on-save-command ()
    (interactive)
    (add-hook 'before-save-hook
              (defun reform-before-save-hook ()
                (interactive)
                (apply 'reform-current-buffer
                       (append `(,bin) args)))
              nil t)))

(provide 'reform)
;;; reform.el ends here
