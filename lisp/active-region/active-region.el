;; http://stackoverflow.com/questions/5651327/how-to-define-keybinding-inside-a-marked-region

;;; Code:

;;;###autoload
(defvar active-region-mode-map
  (let ((map (make-sparse-keymap)))
    map))

;;;###autoload
(define-minor-mode active-region-mode
  "active region minor mode."
  :init-value nil
  :lighter " Region"
  :keymap active-region-mode-map
  :group 'active-region)

(provide 'active-region)

;;; active-region.el ends here
